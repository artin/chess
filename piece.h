#ifndef PIECE_H
#define PIECE_H

#include <string>
#include <QPushButton>

using namespace std;

class Piece
{
    friend class Tile;
    friend class Game; //remove this shit afterwards.
public:
    Piece();
    virtual void highlight_possible_moves() = 0;
    virtual void flag_checked_tiles() = 0;
    virtual void set_position(int, int) = 0;
    virtual QIcon white_icon() = 0;
    virtual QIcon white_selected_icon() = 0;
    virtual QIcon black_icon() = 0;
    virtual QIcon black_selected_icon() = 0;
    virtual QIcon white_kill_icon() = 0;
    virtual QIcon black_kill_icon() = 0;
    virtual ~Piece() = 0;

private:
    int position[2];
    bool is_at_initial_position;
    bool is_alive;
    bool is_white;
    string piece_type;
};

#endif // PIECE_H
