#include "tile.h"
#include "game.h"

Tile::Tile(QPushButton *parent) :
    QPushButton(parent)
{
    is_selected = false;
    is_selectable = true;
    piece = NULL;
    is_a_possible_move = false;
    is_a_possible_kill = false;
}

Piece *Tile::tile_address()
{
    return this->piece;
}

void Tile::set_position(int input_row, int input_column)
{
    position[0] = input_row;
    position[1] = input_column;
}

void Tile::select(bool is_true)
{
    if (is_true)
    {
        is_selected = true;
        if (this->piece->is_white)
        {
            this->setIcon(this->piece->white_selected_icon());
        }
        if (!this->piece->is_white)
        {
            this->setIcon(this->piece->black_selected_icon());
        }
    }
    if (!is_true)
    {
        is_selected = false;
        QPixmap default_tile("default_tile.png");
        QIcon DefaultTile(default_tile);
        this->setIcon(DefaultTile);
    }
}

void Tile::mousePressEvent(QMouseEvent *)
{
    if (!this->is_selected && this->is_selectable)
    {
        if (this->is_a_possible_move || this->is_a_possible_kill)
        {
            if (this->is_a_possible_move)
            {
                for (int row = 0; row < BORDER; row++)
                {
                    for (int column = 0; column < BORDER; column++)
                    {
                        if (Game::get_game_instance()->tile[row][column].is_selected)
                        {
                            swap(this->position, Game::get_game_instance()->tile[row][column].piece->is_white,
                                 Game::get_game_instance()->tile[row][column].piece->piece_type);
                            this->piece->is_white = Game::get_game_instance()->tile[row][column].piece->is_white;
                            Game::get_game_instance()->end_turn();
                            Game::get_game_instance()->tile[row][column].select(false);
                            this->enable_tiles(true);
                            delete Game::get_game_instance()->tile[row][column].piece;
                            Game::get_game_instance()->tile[row][column].piece = NULL;
                            Game::get_game_instance()->tile[row][column].is_selectable = false;
                            this->is_selectable = true;
                            this->setEnabled(true);
                            Game::get_game_instance()->refresh_board();
                        }
                    }
                }
            }
            else if (this->is_a_possible_kill)
            {
                for (int row = 0; row < BORDER; row++)
                {
                    for (int column = 0; column < BORDER; column++)
                    {
                        if (Game::get_game_instance()->tile[row][column].is_selected)
                        {
                            delete this->piece;
                            this->piece = NULL;
                            swap_kill(this->position, Game::get_game_instance()->tile[row][column].piece->is_white,
                                      Game::get_game_instance()->tile[row][column].piece->piece_type);
                            if (Game::get_game_instance()->is_white_players_turn() &&
                                    Game::get_game_instance()->black_check_status())
                            {
                                //show black is checked;
                            }
                            if (!Game::get_game_instance()->is_white_players_turn() &&
                                    Game::get_game_instance()->white_check_status())
                            {
                                //show check white is checked;
                            }
//                            if (Game::get_game_instance()->is_white_players_turn() &&
//                                    Game::get_game_instance()->black_checkmate_status())
//                            {
//                                //show black-win game-over screen;
//                            }
//                            if (!Game::get_game_instance()->is_white_players_turn() &&
//                                    Game::get_game_instance()->white_checkmate_status())
//                            {
//                                //show white-win game-over screen;
//                            }
                            this->piece->is_white = Game::get_game_instance()->tile[row][column].piece->is_white;
                            Game::get_game_instance()->end_turn();
                            Game::get_game_instance()->tile[row][column].select(false);
                            this->enable_tiles(true);
                            delete Game::get_game_instance()->tile[row][column].piece;
                            Game::get_game_instance()->tile[row][column].piece = NULL;
                            Game::get_game_instance()->tile[row][column].is_selectable = false;
                            this->is_selectable = true;
                            this->setEnabled(true);
                            Game::get_game_instance()->refresh_board();
                        }
                    }
                }
            }
        }
        else if (this->is_selectable)
        {
            this->select(true);
            this->enable_tiles(false);
            this->piece->highlight_possible_moves();
            for (int row = 0; row < BORDER; row++)
            {
                for (int column = 0; column < BORDER; column++)
                {
                    if (Game::get_game_instance()->tile[row][column].is_a_possible_move)
                    {
//                        swap(Game::get_game_instance()->tile[row][column].position, this->piece->is_white, this->piece->piece_type);
////                        Game::get_game_instance()->end_turn();
//                        Game::get_game_instance()->tile[row][column].piece->is_white = this->piece->is_white;
//                        Game::get_game_instance()->tile[row][column].piece->piece_type = this->piece->piece_type; //recently added.
////                        Game::get_game_instance()->tile[row][column].select(false);
////                        this->enable_tiles(true);
//                        delete this->piece;
//                        this->piece = NULL;
//                        this->is_selectable = false;
//                        Game::get_game_instance()->tile[row][column].is_selectable = true;
//                        Game::get_game_instance()->tile[row][column].setEnabled(true);
//                        if (Game::get_game_instance()->tile[row][column].piece->is_white)
//                        {
//                            if (!Game::get_game_instance()->white_check_status())
////                                Game::get_game_instance()->tile[row][column].unmake_possible_move(); //cm'ed until checked.
//                            swap(this->position, Game::get_game_instance()->tile[row][column].piece->is_white,
//                                 Game::get_game_instance()->tile[row][column].piece->piece_type);
////                            Game::get_game_instance()->end_turn();
//                            this->piece->is_white = Game::get_game_instance()->tile[row][column].piece->is_white;
//                            this->piece->piece_type = Game::get_game_instance()->tile[row][column].piece->piece_type; //recently added.
////                            Game::get_game_instance()->tile[row][column].select(false);
////                            this->enable_tiles(true);
//                            delete Game::get_game_instance()->tile[row][column].piece;
//                            Game::get_game_instance()->tile[row][column].piece = NULL;
//                            Game::get_game_instance()->tile[row][column].is_selectable = false;
//                            this->is_selectable = true;
//                            this->setEnabled(true);
//                            Game::get_game_instance()->refresh_board();
//                        }
//                        if (!Game::get_game_instance()->tile[row][column].piece->is_white)
//                        {
//                            if (!Game::get_game_instance()->black_check_status())
////                                Game::get_game_instance()->tile[row][column].unmake_possible_move(); cm'ed until checked.
//                            swap(this->position, Game::get_game_instance()->tile[row][column].piece->is_white,
//                                 Game::get_game_instance()->tile[row][column].piece->piece_type);
////                            Game::get_game_instance()->end_turn();
//                            this->piece->is_white = Game::get_game_instance()->tile[row][column].piece->is_white;
//                            this->piece->piece_type = Game::get_game_instance()->tile[row][column].piece->piece_type;
////                            Game::get_game_instance()->tile[row][column].select(false);
////                            this->enable_tiles(true);
//                            delete Game::get_game_instance()->tile[row][column].piece;
//                            Game::get_game_instance()->tile[row][column].piece = NULL;
//                            Game::get_game_instance()->tile[row][column].is_selectable = false;
//                            this->is_selectable = true;
//                            this->setEnabled(true);
//                            Game::get_game_instance()->refresh_board();
//                        }
//                        Game::get_game_instance()->refresh_board();
                    }
//                }
//            }
//            for (int row = 0; row < BORDER; row++)
//            {
//                for (int column = 0; column < BORDER; column++)
//                {
//                    if (Game::get_game_instance()->tile[row][column].is_a_possible_move)
//                        swap(Game::get_game_instance()->tile[row][column].position,
//                                   this->piece->is_white, this->piece->piece_type);
//                            if (Game::get_game_instance()->white_check_status())
//                            {
//                                Game::get_game_instance()->tile[row][column].swap(this->piece->position, this->piece->is_white, this->piece->piece_type);
//                                delete Game::get_game_instance()->tile[row][column].piece;
//                                Game::get_game_instance()->tile[row][column].piece = NULL;
//                            }
//                            this->is_selectable = true;
//                            this->setEnabled(true);
//                            Game::get_game_instance()->refresh_board();
//                            Game::get_game_instance()->tile[row][column].setDisabled(true);

                }
            }
        }
    }
    else if (this->is_selected)
    {
        this->select(false);
        for (int row = 0; row < BORDER; row++)
        {
            for (int column = 0; column < BORDER; column++)
            {
                if (Game::get_game_instance()->tile[row][column].piece)
                {
                    if (Game::get_game_instance()->tile[row][column].piece->is_white)
                    {
                        Game::get_game_instance()->tile[row][column].setIcon(Game::get_game_instance()->tile[row][column].piece->white_icon());
                        if (!this->piece->is_white) //IN CASE OF A BUG, THIS IS THE FIRST PLACE TO LOOK!!!
                        {
                            Game::get_game_instance()->tile[row][column].setDisabled(true);
                        }
                    }
                    if (!Game::get_game_instance()->tile[row][column].piece->is_white)
                    {
                        Game::get_game_instance()->tile[row][column].setIcon(Game::get_game_instance()->tile[row][column].piece->black_icon());
                        if (this->piece->is_white) //AS ABOVE.
                        {
                            Game::get_game_instance()->tile[row][column].setDisabled(true);
                        }
                    }
                }
                if (&Game::get_game_instance()->tile[row][column] == this)
                    continue;
                if (Game::get_game_instance()->tile[row][column].piece)
                {
                    if (Game::get_game_instance()->tile[row][column].piece->is_white && Game::get_game_instance()->is_white_players_turn())
                        Game::get_game_instance()->tile[row][column].setEnabled(true);
                    if (!Game::get_game_instance()->tile[row][column].piece->is_white && !Game::get_game_instance()->is_white_players_turn())
                        Game::get_game_instance()->tile[row][column].setEnabled(true);
                }
                if (Game::get_game_instance()->tile[row][column].piece == NULL)
                {
                    Game::get_game_instance()->tile[row][column].set_default_tile();
                }
            }
        }
        if (this->piece->is_white)
        {
            this->setIcon(this->piece->white_icon());
        }
        if (!this->piece->is_white)
        {
            this->setIcon(this->piece->black_icon());
        }
    }
    if (!(this->is_selected) && this->piece && this->piece->piece_type == "King")
    {
        if (Game::get_game_instance()->white_check_status())
        {
            for (int row = 0; row < BORDER; row++)
            {
                for (int column = 0; column < BORDER; column++)
                {
                    if (Game::get_game_instance()->tile[row][column].piece)
                    {
                        if (Game::get_game_instance()->tile[row][column].piece->piece_type == "King" &&
                                Game::get_game_instance()->tile[row][column].piece->is_white)
                        {
                            Game::get_game_instance()->tile[row][column].setIcon(Game::get_game_instance()->tile[row][column].piece->white_kill_icon());
                        }
                    }
                }
            }
        }
        if (Game::get_game_instance()->black_check_status())
        {
            for (int row = 0; row < BORDER; row++)
            {
                for (int column = 0; column < BORDER; column++)
                {
                    if (Game::get_game_instance()->tile[row][column].piece)
                    {
                        if (Game::get_game_instance()->tile[row][column].piece->piece_type == "King" &&
                                !Game::get_game_instance()->tile[row][column].piece->is_white)
                        {
                            Game::get_game_instance()->tile[row][column].setIcon(Game::get_game_instance()->tile[row][column].piece->black_kill_icon());
                        }
                    }
                }
            }
        }
    }
//    Game::get_game_instance()->board.debug(Game::get_game_instance()->white_check_status(),
//                                           Game::get_game_instance()->black_check_status());

//    if (Game::get_game_instance()->white_checkmate_status() || Game::get_game_instance()->black_checkmate_status())
//        Game::get_game_instance()->board.close();
}

void Tile::swap(int *new_position, bool is_white, string piece_type)
{
    if (piece_type == "Pawn")
    {
        if (is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new Pawn(new_position[0], new_position[1], true, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "Pawn";
        }
        if (!is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new Pawn(new_position[0], new_position[1], false, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "Pawn";
        }
    }
    if (piece_type == "Bishop")
    {
        if (is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new Bishop(new_position[0], new_position[1], true, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "Bishop";
        }
        if (!is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new Bishop(new_position[0], new_position[1], false, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "Bishop";
        }
    }
    if (piece_type == "Knight")
    {
        if (is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new Knight(new_position[0], new_position[1], true, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "Knight";
        }
        if (!is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new Knight(new_position[0], new_position[1], false, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "Knight";
        }
    }
    if (piece_type == "Queen")
    {
        if (is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new Queen(new_position[0], new_position[1], true, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "Queen";
        }
        if (!is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new Queen(new_position[0], new_position[1], false, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "Queen";
        }
    }
    if (piece_type == "Rook")
    {
        if (is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new Rook(new_position[0], new_position[1], true, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "Rook";
        }
        if (!is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new Rook(new_position[0], new_position[1], false, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "Rook";
        }
    }
    if (piece_type == "King")
    {
        if (is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new King(new_position[0], new_position[1], true, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "King";
        }
        if (!is_white)
        {
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece = new King(new_position[0], new_position[1], false, false);
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[0] = new_position[0];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->position[1] = new_position[1];
            Game::get_game_instance()->tile[new_position[0]][new_position[1]].piece->piece_type = "King";
        }
    }
}

void Tile::swap_kill(int *victim_position, bool killer_is_white, string piece_type)
{
    if (piece_type == "Pawn")
    {
        if (killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new Pawn(victim_position[0], victim_position[1], true, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "Pawn";
        }
        if (!killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new Pawn(victim_position[0], victim_position[1], false, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "Pawn";
        }
    }
    if (piece_type == "Bishop")
    {
        if (killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new Bishop(victim_position[0], victim_position[1], true, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "Bishop";
        }
        if (!killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new Bishop(victim_position[0], victim_position[1], false, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "Bishop";
        }
    }
    if (piece_type == "Knight")
    {
        if (killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new Knight(victim_position[0], victim_position[1], true, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "Knight";
        }
        if (!killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new Knight(victim_position[0], victim_position[1], false, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "Knight";
        }
    }
    if (piece_type == "Queen")
    {
        if (killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new Queen(victim_position[0], victim_position[1], true, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "Queen";
        }
        if (!killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new Queen(victim_position[0], victim_position[1], false, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "Queen";
        }
    }
    if (piece_type == "Rook")
    {
        if (killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new Rook(victim_position[0], victim_position[1], true, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "Rook";
        }
        if (!killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new Rook(victim_position[0], victim_position[1], false, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "Rook";
        }
    }
    if (piece_type == "King")
    {
        if (killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new King(victim_position[0], victim_position[1], true, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "King";
        }
        if (!killer_is_white)
        {
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece = new King(victim_position[0], victim_position[1], false, false);
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[0] = victim_position[0];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->position[1] = victim_position[1];
            Game::get_game_instance()->tile[victim_position[0]][victim_position[1]].piece->piece_type = "King";
        }
    }
}

void Tile::enable_tiles(bool is_true)
{
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            if (&Game::get_game_instance()->tile[row][column] == this)
                continue;
            Game::get_game_instance()->tile[row][column].setDisabled(!is_true);
            if (Game::get_game_instance()->tile[row][column].piece == NULL)
            {
                Game::get_game_instance()->tile[row][column].set_default_tile();
            }
        }
    }
}

void Tile::set_possible_move(bool is_true)
{
    if (is_true)
        this->is_a_possible_move = true;
    if (!is_true)
        is_a_possible_move = false;
}

void Tile::make_possible_move()
{
    this->setEnabled(true);
    set_possible_move(true);
    this->is_selectable = true;
    QPixmap selectable_tile("selectable_tile.png");
    QIcon SelectableTile(selectable_tile);
    this->setIcon(SelectableTile);
}

void Tile::unmake_possible_move()
{
    this->setDisabled(true);
    set_possible_move(false);
    this->is_selectable = false;
    QPixmap default_tile("default_tile.png");
    QIcon DefaultTile(default_tile);
    this->setIcon(DefaultTile);
}


void Tile::set_possible_kill(bool is_true)
{
    if (is_true)
    {
        this->is_a_possible_kill = true;
        {
            if (this->piece->is_white)
            {
                this->setIcon(this->piece->white_kill_icon());
            }
            if (!this->piece->is_white)
            {
                this->setIcon(this->piece->black_kill_icon());
            }
        }
    }
    if (!is_true)
    {
        this->is_a_possible_kill = false;
        if (this->piece)
        {
            if (this->piece->is_white)
            {
                this->setIcon(this->piece->white_icon());
            }
            if (!this->piece->is_white)
            {
                this->setIcon(this->piece->black_icon());
            }
        }
    }
}

void Tile::make_possible_kill(string piece_type)
{
    this->setEnabled(true);
    set_possible_kill(true);
    this->is_selectable = true;
}

void Tile::set_default_tile()
{
    QPixmap default_tile("default_tile.png");
    QIcon DefaultTile(default_tile);
    this->setIcon(DefaultTile);
}

bool Tile::is_white()
{
    return (this->piece->is_white);
}

bool Tile::has_a_piece()
{
    if (this->piece)
        return true;
    else return false;
}

void Tile::set_check_flag(bool is_true)
{
    if (is_true)
        this->check_flag = true;
    else this->check_flag = false;
}

bool Tile::is_a_bad_move(int *old_position)
{
    bool bad_move = false;
    if (!this->piece)
    {
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "Pawn")
        {
            this->piece = new Pawn(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "Rook")
        {
            this->piece = new Rook(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "Knight")
        {
            this->piece = new Knight(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "Bishop")
        {
            this->piece = new Bishop(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "Queen")
        {
            this->piece = new Queen(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "King")
        {
            this->piece = new King(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        this->piece->is_white = Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white;
        this->piece->piece_type = Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type;
        this->piece->is_at_initial_position = Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position;
//                delete[] Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece;
        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = NULL; //I'm still skeptical as to whether it's really necessary.
        if (this->piece->is_white)
        {
            bad_move = Game::get_game_instance()->white_check_status();
        }
        if (!this->piece->is_white)
        {
            bad_move = Game::get_game_instance()->black_check_status();
        }
        //Now, off to reverse the procedure.
        if (this->piece->piece_type == "Pawn")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new Pawn(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Rook")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new Rook(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Knight")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new Knight(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Bishop")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new Bishop(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Queen")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new Queen(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "King")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new King(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white = this->piece->is_white;
        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type = this->piece->piece_type;
        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position = this->piece->is_at_initial_position;
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white)
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].setIcon(
                        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->white_selected_icon());
        }
        if (!Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white)
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].setIcon(
                        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->black_selected_icon());
        }
//        delete[] this->piece;
        this->piece = NULL;
        //Procedure reversed. Now what about icons? :-?
        QPixmap default_tile("default_tile.png");
        QIcon DefaultTile(default_tile);
        this->setIcon(DefaultTile);
        //I hope this happens so fast that it's undetectable to the unarmed eye.
    }
    if (this->piece)
    {
        Piece *temp;
        if (this->piece->piece_type == "Pawn")
        {
            temp = new Pawn(this->position[0], this->position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Rook")
        {
            temp = new Rook(this->position[0], this->position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Knight")
        {
            temp = new Knight(this->position[0], this->position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Bishop")
        {
            temp = new Bishop(this->position[0], this->position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Queen")
        {
            temp = new Pawn(this->position[0], this->position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "King")
        {
            temp = new King(this->position[0], this->position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        temp->is_white = this->piece->is_white;
        temp->piece_type = this->piece->piece_type;
        this->piece = NULL;
        delete[] this->piece;
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "Pawn")
        {
            this->piece = new Pawn(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "Rook")
        {
            this->piece = new Rook(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "Knight")
        {
            this->piece = new Knight(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "Bishop")
        {
            this->piece = new Bishop(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "Queen")
        {
            this->piece = new Queen(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type == "King")
        {
            this->piece = new King(this->position[0], this->position[1],
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white,
                    Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position);
        }
        this->piece->is_white = Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white;
        this->piece->piece_type = Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type;
        this->piece->is_at_initial_position = Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position;
//        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white)
//        {
//            Game::get_game_instance()->tile[old_position[0]][old_position[1]].setIcon(
//                        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->white_selected_icon());
//        }
//        if (!Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white)
//        {
//            Game::get_game_instance()->tile[old_position[0]][old_position[1]].setIcon(
//                        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->black_selected_icon());
//        }
        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = NULL;
//        this->piece = NULL;
        if (this->piece->is_white)
        {
            if (Game::get_game_instance()->white_check_status())
                bad_move = true;
            else bad_move = false;
        }
        if (!this->piece->is_white)
        {
            if (Game::get_game_instance()->black_check_status())
                bad_move = true;
            else bad_move = false;
        }

        if (this->piece->piece_type == "Pawn")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new Pawn(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Rook")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new Rook(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Knight")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new Knight(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Bishop")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new Bishop(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "Queen")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new Queen(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
        }
        if (this->piece->piece_type == "King")
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece = new King(old_position[0], old_position[1], this->piece->is_white, this->piece->is_at_initial_position);
            //Maybe 'tis better to return false at this point right away?
        }
        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->position[0] = old_position[0];
        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->position[1] = old_position[1];
        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white = this->piece->is_white;
        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->piece_type = this->piece->piece_type;
        Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_at_initial_position = this->piece->is_at_initial_position;

        this->piece = NULL;

        if (temp->piece_type == "Pawn")
        {
            this->piece = new Pawn(this->position[0], this->position[1], temp->is_white, temp->is_at_initial_position);
        }
        if (temp->piece_type == "Rook")
        {
            this->piece = new Rook(this->position[0], this->position[1], temp->is_white, temp->is_at_initial_position);
        }
        if (temp->piece_type == "Knight")
        {
            this->piece = new Knight(this->position[0], this->position[1], temp->is_white, temp->is_at_initial_position);
        }
        if (temp->piece_type == "Bishop")
        {
            this->piece = new Bishop(this->position[0], this->position[1], temp->is_white, temp->is_at_initial_position);
        }
        if (temp->piece_type == "Queen")
        {
            this->piece = new Queen(this->position[0], this->position[1], temp->is_white, temp->is_at_initial_position);
        }
        if (temp->piece_type == "King")
        {
            this->piece = new King(this->position[0], this->position[1], temp->is_white, temp->is_at_initial_position);
        }
        this->piece->is_white = temp->is_white;
        this->piece->piece_type = temp->piece_type;
        this->piece->position[0] = this->position[0];
        this->piece->position[1] = this->position[1];
        delete[] temp;
        if (Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white)
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].setIcon(Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->white_selected_icon());
        }
        if (!Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->is_white)
        {
            Game::get_game_instance()->tile[old_position[0]][old_position[1]].setIcon(Game::get_game_instance()->tile[old_position[0]][old_position[1]].piece->black_selected_icon());
        }
    }
    return bad_move;
}
