#include "registration.h"
#include "ui_registration.h"
#include "game.h"

Registration::Registration(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Registration)
{
    ui->setupUi(this);
}

Registration::~Registration()
{
    delete ui;
}

void Registration::on_pushButton_2_released()
{
    this->close();
}

void Registration::on_pushButton_released()
{
    Game::get_game_instance()->setup_board();
    Game::get_game_instance()->set_player_names(this->ui->player1_name->text(), this->ui->player2_name->text());
    this->close();
}
