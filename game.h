#ifndef GAME_H
#define GAME_H

#define BORDER 8

//#include "timer.h"
#include "piece.h"
#include "tile.h"
#include "widget.h"
#include "pawn.h"
#include "rook.h"
#include "knight.h"
#include "bishop.h"
#include "queen.h"
#include "king.h"

class Game
{
    friend class Tile;
    friend class Pawn;
    friend class Rook;
    friend class Knight;
    friend class Bishop;
    friend class Queen;
    friend class King;
public:
    friend class Widget;
    static Game *get_game_instance();
    void do_nothing(){}
    void setup_board();
    void set_icon(QIcon, int, int);
    void refresh_board();
    bool is_white_players_turn();
    void end_turn();
    bool white_check_status();
    bool white_checkmate_status();
    bool black_check_status();
    bool black_checkmate_status();
    void set_player_names(QString, QString);

private:
    Game();
    static Game *game_instance;
    Tile **tile;
    Widget board;
    bool is_white_turn;
    bool is_white_checked;
    bool is_white_checkmated;
    bool is_black_checked;
    bool is_black_checkmated;
    QString player1_name;
    QString player2_name;
    QPushButton white_dead_pieces[16];
    QPushButton black_dead_pieces[16];
};

#endif // GAME_H
