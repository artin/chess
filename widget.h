#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QColor>
//#include "timer.h"
#include "registration.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT
    friend class Piece;
    friend class Widget;

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void setup_buttons();
    void setup_dead_pieces();
    void set_player_names();
    void debug(int, int);
    void set_bold_player1(bool);
    void set_color();
//    Timer timer;

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
