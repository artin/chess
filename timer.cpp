#include "timer.h"

Timer::Timer(QWidget *parent) :
    QWidget(parent)
{
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000);

    setWindowTitle(tr("Analog Timer"));
    resize(100, 100);
}

void Timer::paintEvent(QPaintEvent *)
{
    static const QPoint hourHand[3] = {
        QPoint(7, 8), QPoint(-7,8), QPoint(0, -40)
    };
    static const QPoint minuteHand[3] = {
        QPoint(7, 8), QPoint(-7, 8), QPoint(0, -70)
    };

    QColor hourColor(127, 0, 127);
    QColor minuteColor(0, 127, 127, 191);

    int side = qMin(width(), height());
//    QTime time = QTime::currentTime();
}

