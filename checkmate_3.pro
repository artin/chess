#-------------------------------------------------
#
# Project created by QtCreator 2013-11-15T23:29:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = checkmate_3
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    tile.cpp \
    game.cpp \
    pawn.cpp \
    rook.cpp \
    knight.cpp \
    bishop.cpp \
    queen.cpp \
    king.cpp \
    piece.cpp \
    registration.cpp \
    timer.cpp

HEADERS  += widget.h \
    tile.h \
    game.h \
    pawn.h \
    rook.h \
    knight.h \
    bishop.h \
    queen.h \
    king.h \
    piece.h \
    registration.h \
    timer.h

FORMS    += widget.ui \
    registration.ui
