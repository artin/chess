#include "game.h"

Game *Game::game_instance = NULL;

Game::Game()
{
}

Game *Game::get_game_instance()
{
    if (!game_instance)
    {
        game_instance = new Game;

        Game::get_game_instance()->tile = new Tile*[BORDER];
        for (int index = 0; index < BORDER; index++)
            Game::get_game_instance()->tile[index] = new Tile[BORDER];
        for (int row = 0; row < BORDER; row++)
        {
            for (int column = 0; column < BORDER; column++)
                Game::get_game_instance()->tile[row][column].set_position(row, column);
        }
    }
    return game_instance;
}

void Game::setup_board()
{
    board.setup_buttons();
//    board.setup_dead_pieces();
    board.set_player_names();
    board.show();
    QPixmap default_tile("default_tile.png");
    QIcon EmptyTile(default_tile);

    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            tile[row][column].position[0] = row;
            tile[row][column].position[1] = column;
//            tile[row][column].piece = NULL;

            if (row == 2 || row == 3 || row == 4 || row == 5)
            {
                Game::get_game_instance()->tile[row][column].setIcon(EmptyTile);
                Game::get_game_instance()->tile[row][column].setDisabled(true);
            }
            if (row == 6)
            {
                Game::get_game_instance()->tile[row][column].piece = new Pawn(row, column, true, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = true;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "Pawn";
            }
            if (row == 1)
            {
                Game::get_game_instance()->tile[row][column].piece = new Pawn(row, column, false, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = false;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "Pawn";
                Game::get_game_instance()->tile[row][column].setDisabled(true);
            }
            if (row == 7 && (column == 0 || column == 7))
            {
                Game::get_game_instance()->tile[row][column].piece = new Rook(row, column, true, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = true;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "Rook";
            }
            if (row == 0 && (column == 0 || column == 7))
            {
                Game::get_game_instance()->tile[row][column].piece = new Rook(row, column, false, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = false;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "Rook";
                Game::get_game_instance()->tile[row][column].setDisabled(true);
            }
            if (row == 7 && (column == 1 || column == 6))
            {
                Game::get_game_instance()->tile[row][column].piece = new Knight(row, column, true, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = true;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "Knight";
            }
            if (row == 0 && (column == 1 || column == 6))
            {
                Game::get_game_instance()->tile[row][column].piece = new Knight(row, column, false, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = false;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "Knight";
                Game::get_game_instance()->tile[row][column].setDisabled(true);
            }
            if (row == 7 && (column == 2 || column == 5))
            {
                Game::get_game_instance()->tile[row][column].piece = new Bishop(row, column, true, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = true;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "Bishop";

            }
            if (row == 0 && (column == 2 || column == 5))
            {
                Game::get_game_instance()->tile[row][column].piece = new Bishop(row, column, false, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = false;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "Bishop";
                Game::get_game_instance()->tile[row][column].setDisabled(true);
            }
            if (row == 7 && column == 3)
            {
                Game::get_game_instance()->tile[row][column].piece = new Queen(row, column, true, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = true;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "Queen";

            }
            if (row == 0 && column == 3)
            {
                Game::get_game_instance()->tile[row][column].piece = new Queen(row, column, false, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = false;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "Queen";
                Game::get_game_instance()->tile[row][column].setDisabled(true);
            }
            if (row == 7 && column == 4)
            {
                Game::get_game_instance()->tile[row][column].piece = new King(row, column, true, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = true;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "King";

            }
            if (row == 0 && column == 4)
            {
                Game::get_game_instance()->tile[row][column].piece = new King(row, column, false, true);
                Game::get_game_instance()->tile[row][column].piece->is_white = false;
                Game::get_game_instance()->tile[row][column].piece->piece_type = "King";
                Game::get_game_instance()->tile[row][column].setDisabled(true);
            }
            Game::get_game_instance()->tile[row][column].setCheckable(true);
            Game::get_game_instance()->tile[row][column].setFlat(true);
        }
    }
    for (int index = 0; index < 16; index++)
    {
        white_dead_pieces[index].setIcon(EmptyTile);
        white_dead_pieces[index].setDisabled(true);
        black_dead_pieces[index].setIcon(EmptyTile);
        black_dead_pieces[index].setDisabled(true);
    }
    Game::get_game_instance()->is_white_turn = true;
}

void Game::set_icon(QIcon piece_icon, int row, int column)
{
    Game::get_game_instance()->tile[row][column].setIcon(piece_icon);
}

void Game::refresh_board()
{
    board.set_player_names();
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            tile[row][column].set_possible_move(false);
            tile[row][column].set_possible_kill(false);
            if (!tile[row][column].piece)
                tile[row][column].setDisabled(true);
            else
            {
//                tile[row][column].setEnabled(true);
                if (this->is_white_players_turn())
                {
                    if (this->tile[row][column].piece->is_white)
                        this->tile[row][column].setEnabled(true);
                    if (!this->tile[row][column].piece->is_white)
                        this->tile[row][column].setDisabled(true);
                }
                if (!this->is_white_players_turn())
                {
                    if (this->tile[row][column].piece->is_white)
                        this->tile[row][column].setDisabled(true);
                    if (!this->tile[row][column].piece->is_white)
                        this->tile[row][column].setEnabled(true);
                }
            }
        }
    }
    if (Game::get_game_instance()->white_check_status())
    {
        for (int row = 0; row < BORDER; row++)
        {
            for (int column = 0; column < BORDER; column++)
            {
                if (Game::get_game_instance()->tile[row][column].piece)
                {
                    if (Game::get_game_instance()->tile[row][column].piece->piece_type == "King" &&
                            Game::get_game_instance()->tile[row][column].piece->is_white)
                    {
                        Game::get_game_instance()->tile[row][column].setIcon(Game::get_game_instance()->tile[row][column].piece->white_kill_icon());
                    }
                }
            }
        }
    }
    if (Game::get_game_instance()->black_check_status())
    {
        for (int row = 0; row < BORDER; row++)
        {
            for (int column = 0; column < BORDER; column++)
            {
                if (Game::get_game_instance()->tile[row][column].piece)
                {
                    if (Game::get_game_instance()->tile[row][column].piece->piece_type == "King" &&
                            !Game::get_game_instance()->tile[row][column].piece->is_white)
                    {
                        Game::get_game_instance()->tile[row][column].setIcon(Game::get_game_instance()->tile[row][column].piece->black_kill_icon());
                    }
                }
            }
        }
    }
}

bool Game::is_white_players_turn()
{
    return this->is_white_turn;
}

void Game::end_turn()
{
    if (white_checkmate_status() || black_checkmate_status())
        board.close();
    if (this->is_white_turn)
    {
        is_white_turn = false;
        Game::get_game_instance()->board.set_bold_player1(false);
        if (Game::get_game_instance()->black_check_status())
        {
            for (int row = 0; row < BORDER; row++)
            {
                for (int column = 0; column < BORDER; column++)
                {
                    if (Game::get_game_instance()->tile[row][column].piece)
                    {
                        if (Game::get_game_instance()->tile[row][column].piece->piece_type == "King" &&
                                !Game::get_game_instance()->tile[row][column].piece->is_white)
                        {
                            Game::get_game_instance()->tile[row][column].setIcon(Game::get_game_instance()->tile[row][column].piece->black_kill_icon());
                        }
                    }
                }
            }
        }
    }
    else
    {
        is_white_turn = true;
        Game::get_game_instance()->board.set_bold_player1(true);
        if (Game::get_game_instance()->white_check_status())
        {
            for (int row = 0; row < BORDER; row++)
            {
                for (int column = 0; column < BORDER; column++)
                {
                    if (Game::get_game_instance()->tile[row][column].piece)
                    {
                        if (Game::get_game_instance()->tile[row][column].piece->piece_type == "King" &&
                                Game::get_game_instance()->tile[row][column].piece->is_white)
                        {
                            Game::get_game_instance()->tile[row][column].setIcon(Game::get_game_instance()->tile[row][column].piece->white_kill_icon());
                        }
                    }
                }
            }
        }
    }
}

bool Game::white_check_status()
{
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            this->tile[row][column].set_check_flag(false);
        }
    }
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            if (this->tile[row][column].piece)
            {
                if (!this->tile[row][column].piece->is_white)
                    this->tile[row][column].piece->flag_checked_tiles();
            }
        }
    }
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            if (this->tile[row][column].piece)
            {
                if (this->tile[row][column].piece->is_white && this->tile[row][column].piece->piece_type == "King")
                {
                    if (this->tile[row][column].check_flag)
                        return true;
                }
            }
        }
    }
    return false;
}

bool Game::black_check_status()
{
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            this->tile[row][column].set_check_flag(false);
        }
    }
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            if (this->tile[row][column].piece)
            {
                if (this->tile[row][column].piece->is_white)
                    this->tile[row][column].piece->flag_checked_tiles();
            }
        }
    }
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            if (this->tile[row][column].piece)
            {
                if (!this->tile[row][column].piece->is_white && this->tile[row][column].piece->piece_type == "King")
                {
                    if (this->tile[row][column].check_flag)
                        return true;
                }
            }
        }
    }
    return false;
}

bool Game::white_checkmate_status()
{//should check every possible move of king as well as other pieces that may lift the check. Should they all fail, only afterwards should it claim victoy.
//    for (int row = 0; row < BORDER; row++)
//    {
//        for (int column = 0; column < BORDER; column++)
//        {
//            this->tile[row][column].set_check_flag(false);
//        }
//    }
//    for (int row = 0; row < BORDER; row++)
//    {
//        for (int column = 0; column < BORDER; column++)
//        {
//            if (this->tile[row][column].piece)
//            {
//                if (!this->tile[row][column].piece->is_white)
//                    this->tile[row][column].piece->flag_checked_tiles();
//            }
//        }
//    }
//    for (int row = 0; row < BORDER; row++)
//    {
//        for (int column = 0; column < BORDER; column++)
//        {
//            if (this->tile[row][column].piece)
//            {
//                if (this->tile[position[0]][position[1]].check_flag && this->white_check_status())
//                    return true;
//            }
//        }
//    }
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            if (tile[row][column].piece)
            {
                if(tile[row][column].piece->is_white)
                {
                    tile[row][column].piece->highlight_possible_moves();
                }
            }
        }
    }
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            if (tile[row][column].is_a_possible_move || tile[row][column].is_a_possible_kill)
            {
                return false;
            }
        }
    }
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            tile[row][column].is_a_possible_move = false;
            if (tile[row][column].is_a_possible_move)
            {
                QPixmap default_tile("default_tile.png");
                QIcon DefaultTile(default_tile);
                tile[row][column].setIcon(DefaultTile);
            }
            if (tile[row][column].piece && tile[row][column].piece->piece_type == "King" && tile[row][column].piece->is_white)
            {
                tile[row][column].setIcon(tile[row][column].piece->white_kill_icon());
            }
            if (tile[row][column].is_a_possible_kill)
            {
                tile[row][column].is_a_possible_kill = false;
                tile[row][column].setIcon(tile[row][column].piece->black_icon());
            }
        }
    }
    return true;
}

bool Game::black_checkmate_status()
{
//    for (int row = 0; row < BORDER; row++)
//    {
//        for (int column = 0; column < BORDER; column++)
//        {
//            this->tile[row][column].set_check_flag(false);
//        }
//    }
//    for (int row = 0; row < BORDER; row++)
//    {
//        for (int column = 0; column < BORDER; column++)
//        {
//            if (this->tile[row][column].piece)
//            {
//                if (this->tile[row][column].piece->is_white)
//                    this->tile[row][column].piece->flag_checked_tiles();
//            }
//        }
//    }
//    for (int row = 0; row < BORDER; row++)
//    {
//        for (int column = 0; column < BORDER; column++)
//        {
//            if (this->tile[row][column].piece)
//            {
//                if (this->tile[position[0]][position[1]].check_flag && this->black_check_status())
//                    return true;
//            }
//        }
//    }


    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            if (tile[row][column].piece)
            {
                if(!tile[row][column].piece->is_white)
                {
                    tile[row][column].piece->highlight_possible_moves();
                }
            }
        }
    }
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            if (tile[row][column].is_a_possible_move || tile[row][column].is_a_possible_kill)
            {
                return false;
            }
        }
    }
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            if (tile[row][column].is_a_possible_move)
            {
                tile[row][column].is_a_possible_move = false;
                QPixmap default_tile("default_tile.png");
                QIcon DefaultTile(default_tile);
                tile[row][column].setIcon(DefaultTile);
            }
            if (tile[row][column].is_a_possible_kill)
            {
                tile[row][column].is_a_possible_kill = false;
                tile[row][column].setIcon(tile[row][column].piece->white_icon());
            }
        }
    }
    return true;
}

void Game::set_player_names(QString player1, QString player2)
{
    Game::get_game_instance()->player1_name = player1;
    Game::get_game_instance()->player2_name = player2;
}
