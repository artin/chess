#include "game.h"
#include "registration.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Registration registration_window;
    registration_window.show();

    return a.exec();
}
