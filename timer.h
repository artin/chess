#ifndef TIMER_H
#define TIMER_H

#include <QWidget>
#include <Qtimer>

class Timer : public QWidget
{
    Q_OBJECT
public:
    explicit Timer(QWidget *parent = 0);
protected:
    void paintEvent(QPaintEvent *event);

signals:

public slots:

};

#endif // TIMER_H
