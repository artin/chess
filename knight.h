#ifndef KNIGHT_H
#define KNIGHT_H

#include "piece.h"
#include "tile.h"

class Knight : public Piece
{
public:
    Knight(int, int, bool, bool);
    void highlight_possible_moves();
    void flag_checked_tiles();
    void set_position(int, int);
    QIcon white_icon();
    QIcon white_selected_icon();
    QIcon black_icon();
    QIcon black_selected_icon();
    QIcon white_kill_icon();
    QIcon black_kill_icon();
    ~Knight();

private:
    int position[2];
    bool is_at_initial_position;
    bool is_alive;
    bool is_white;
    string piece_type;
};

#endif // KNIGHT_H
