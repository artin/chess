#include "king.h"
#include "game.h"

King::King(int row, int column, bool is_white, bool is_at_initial_position)
{ 
    if (is_white)
    {
        Game::get_game_instance()->set_icon(white_icon(), row, column);
        this->is_white = true;
    }

    if (!is_white)
    {
        Game::get_game_instance()->set_icon(black_icon(), row, column);
        this->is_white = false;
    }
    this->position[0] = row;
    this->position[1] = column;
    this->is_alive = true;
    this->is_at_initial_position = is_at_initial_position;
    this->piece_type = "King";
}

QIcon King::white_icon()
{
    QPixmap white_king("white_king.png");
    QIcon WhiteKing(white_king);
    return WhiteKing;
}

QIcon King::white_selected_icon()
{
    QPixmap white_selected_king("selected_white_king.png");
    QIcon WhiteSelectedKing(white_selected_king);
    return WhiteSelectedKing;
}

QIcon King::black_icon()
{
    QPixmap black_king("black_king.png");
    QIcon BlackKing(black_king);
    return BlackKing;
}

QIcon King::black_selected_icon()
{
    QPixmap black_selected_king("selected_black_king.png");
    QIcon BlackSelectedKing(black_selected_king);
    return BlackSelectedKing;
}

QIcon King::white_kill_icon()
{
    QPixmap white_kill_king("kill_white_king.png");
    QIcon WhiteKillKing(white_kill_king);
    return WhiteKillKing;
}

QIcon King::black_kill_icon()
{
    QPixmap black_kill_king("kill_black_king.png");
    QIcon BlackKillKing(black_kill_king);
    return BlackKillKing;
}

void King::set_position(int row, int column)
{
    position[0] = row;
    position[1] = column;
}

void King::highlight_possible_moves()
{
    if(this->position[0]-1 < BORDER && this->position[0]-1 >= 0 && this->position[1] < BORDER && this->position[1] >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].piece)
        {
            if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].make_possible_move();
            }
        }
        if(Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].piece)
            if ((Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].is_white() &&
                 !(this->is_white)) || (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].is_white() &&
                                      this->is_white))
            {
                if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].make_possible_kill("0");
                }
            }
    }

    if(this->position[0]-1 < BORDER && this->position[0]-1 >= 0 && this->position[1]+1 < BORDER && this->position[1]+1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].piece)
        {
            if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].make_possible_move();
            }
        }
        if(Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].is_white() &&
                                      this->is_white))
            {
                if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].make_possible_kill("0");
                }
            }
    }

    if(this->position[0] < BORDER && this->position[0] >= 0 && this->position[1]+1 < BORDER && this->position[1]+1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].piece)
        {
            if (!Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].make_possible_move();
            }
        }
        if(Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].is_white() &&
                                      this->is_white))
            {
                if (!Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].make_possible_kill("0");
                }
            }
    }

    if(this->position[0]+1 < BORDER && this->position[0]+1 >= 0 && this->position[1]+1 < BORDER && this->position[1]+1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].piece)
        {
            if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].make_possible_move();
            }
        }
        if(Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].is_white() &&
                                      this->is_white))
            {
                if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].make_possible_kill("0");
                }
            }
    }

    if(this->position[0]+1 < BORDER && this->position[0]+1 >= 0 && this->position[1] < BORDER && this->position[1] >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].piece)
        {
            if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].make_possible_move();
            }
        }
        if(Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].piece)
            if ((Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].is_white() &&
                                      this->is_white))
            {
                if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].make_possible_kill("0");
                }
            }
    }

    if(this->position[0]+1 < BORDER && this->position[0]+1 >= 0 && this->position[1]-1 < BORDER && this->position[1]-1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].piece)
        {
            if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].make_possible_move();
            }
        }
        if(Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].is_white() &&
                                      this->is_white))
            {
                if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].make_possible_kill("0");
                }
            }
    }

    if(this->position[0] < BORDER && this->position[0] >= 0 && this->position[1]-1 < BORDER && this->position[1]-1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].piece)
        {
            if (!Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].make_possible_move();
            }
        }
        if(Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].is_white() &&
                                      this->is_white))
            {
                if (!Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].make_possible_kill("0");
                }
            }
    }

    if(this->position[0]-1 < BORDER && this->position[0]-1 >= 0 && this->position[1]-1 < BORDER && this->position[1]-1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].piece)
        {
            if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].make_possible_move();
            }
        }
        if(Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_white() &&
                                      this->is_white))
            {
                if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].make_possible_kill("0");
                }
            }
    }
}

void King::flag_checked_tiles()
{
    if(this->position[0]-1 < BORDER && this->position[0]-1 >= 0 && this->position[1] < BORDER && this->position[1] >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].piece)
            Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].set_check_flag(true);
        if(Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].piece)
            if ((Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].is_white() &&
                 !(this->is_white)) || (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].is_white() &&
                                      this->is_white))
            {
                Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].set_check_flag(true);
            }
    }

    if(this->position[0]-1 < BORDER && this->position[0]-1 >= 0 && this->position[1]+1 < BORDER && this->position[1]+1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].piece)
            Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].set_check_flag(true);
        if(Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].is_white() &&
                                      this->is_white))
            {
                Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].set_check_flag(true);
            }
    }

    if(this->position[0] < BORDER && this->position[0] >= 0 && this->position[1]+1 < BORDER && this->position[1]+1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].piece)
            Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].set_check_flag(true);
        if(Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].is_white() &&
                                      this->is_white))
            {
                Game::get_game_instance()->tile[this->position[0]][this->position[1]+1].set_check_flag(true);
            }
    }

    if(this->position[0]+1 < BORDER && this->position[0]+1 >= 0 && this->position[1]+1 < BORDER && this->position[1]+1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].piece)
            Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].set_check_flag(true);
        if(Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].is_white() &&
                                      this->is_white))
            {
                Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].set_check_flag(true);
            }
    }

    if(this->position[0]+1 < BORDER && this->position[0]+1 >= 0 && this->position[1] < BORDER && this->position[1] >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].piece)
            Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].set_check_flag(true);
        if(Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].piece)
            if ((Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].is_white() &&
                                      this->is_white))
            {
                Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].set_check_flag(true);
            }
    }

    if(this->position[0]+1 < BORDER && this->position[0]+1 >= 0 && this->position[1]-1 < BORDER && this->position[1]-1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].piece)
            Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].set_check_flag(true);
        if(Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].is_white() &&
                                      this->is_white))
            {
                Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].set_check_flag(true);
            }
    }

    if(this->position[0] < BORDER && this->position[0] >= 0 && this->position[1]-1 < BORDER && this->position[1]-1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].piece)
            Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].set_check_flag(true);
        if(Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].is_white() &&
                                      this->is_white))
            {
                Game::get_game_instance()->tile[this->position[0]][this->position[1]-1].set_check_flag(true);
            }
    }

    if(this->position[0]-1 < BORDER && this->position[0]-1 >= 0 && this->position[1]-1 < BORDER && this->position[1]-1 >= 0)
    {
        if(!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].piece)
            Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].set_check_flag(true);
        if(Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].piece)
            if ((Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_white() &&
                 !this->is_white) || (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_white() &&
                                      this->is_white))
            {
                Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].set_check_flag(true);
            }
    }
}

King::~King()
{

}
