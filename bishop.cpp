#include "bishop.h"
#include "game.h"

Bishop::Bishop(int row, int column, bool is_white, bool is_at_initial_position)
{
    if (is_white)
    {
        Game::get_game_instance()->set_icon(white_icon(), row, column);
        this->is_white = true;
    }

    if (!is_white)
    {
        Game::get_game_instance()->set_icon(black_icon(), row, column);
        this->is_white = false;
    }
    this->position[0] = row;
    this->position[1] = column;
    this->is_alive = true;
    this->is_at_initial_position = is_at_initial_position;
    this->piece_type = "Bishop";
}

QIcon Bishop::white_icon()
{
    QPixmap white_bishop("white_bishop.png");
    QIcon WhiteBishop(white_bishop);
    return WhiteBishop;
}

QIcon Bishop::white_selected_icon()
{
    QPixmap white_selected_bishop("selected_white_bishop.png");
    QIcon WhiteSelectedBishop(white_selected_bishop);
    return WhiteSelectedBishop;
}

QIcon Bishop::black_icon()
{
    QPixmap black_bishop("black_bishop.png");
    QIcon BlackBishop(black_bishop);
    return BlackBishop;
}

QIcon Bishop::black_selected_icon()
{
    QPixmap black_selected_bishop("selected_black_bishop.png");
    QIcon BlackSelectedBishop(black_selected_bishop);
    return BlackSelectedBishop;
}

QIcon Bishop::white_kill_icon()
{
    QPixmap white_kill_bishop("kill_white_bishop.png");
    QIcon WhiteKillBishop(white_kill_bishop);
    return WhiteKillBishop;
}

QIcon Bishop::black_kill_icon()
{
    QPixmap black_kill_bishop("kill_black_bishop.png");
    QIcon BlackKillBishop(black_kill_bishop);
    return BlackKillBishop;
}

void Bishop::set_position(int row, int column)
{
    position[0] = row;
    position[1] = column;
}

void Bishop::highlight_possible_moves()
{
    int row = this->position[0];
    int column = this->position[1];

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row + diagonal_length) >= BORDER || (row + diagonal_length) < 0  ||
                (column + diagonal_length) >= BORDER || (column + diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].make_possible_kill("0");
                    }
                }
                break;
            }
            else
            {
                if (!Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].make_possible_move();
                }
            }
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row - diagonal_length) >= BORDER || (row - diagonal_length) < 0  ||
                (column + diagonal_length) >= BORDER || (column + diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].make_possible_kill("0");
                    }
                }
                break;
            }
            else
            {
                if (!Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].make_possible_move();
                }
            }
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row + diagonal_length) >= BORDER || (row + diagonal_length) < 0  ||
                (column - diagonal_length) >= BORDER || (column - diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].make_possible_kill("0");
                    }
                }
                break;
            }
            else
            {
                if (!Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].make_possible_move();
                }
            }
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row - diagonal_length) >= BORDER || (row - diagonal_length) < 0  ||
                (column - diagonal_length) >= BORDER || (column - diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].make_possible_kill("0");
                    }
                }
                break;
            }
            else
            {
                if (!Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].make_possible_move();
                }
            }
        }
    }
}

void Bishop::flag_checked_tiles()
{
    int row = this->position[0];
    int column = this->position[1];

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row + diagonal_length) >= BORDER || (row + diagonal_length) < 0  ||
                (column + diagonal_length) >= BORDER || (column + diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].set_check_flag(true);
                break;
            }
            else Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].set_check_flag(true);
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row - diagonal_length) >= BORDER || (row - diagonal_length) < 0  ||
                (column + diagonal_length) >= BORDER || (column + diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].set_check_flag(true);
                break;
            }
            else Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].set_check_flag(true);
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row + diagonal_length) >= BORDER || (row + diagonal_length) < 0  ||
                (column - diagonal_length) >= BORDER || (column - diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].set_check_flag(true);
                break;
            }
            else Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].set_check_flag(true);
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row - diagonal_length) >= BORDER || (row - diagonal_length) < 0  ||
                (column - diagonal_length) >= BORDER || (column - diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].set_check_flag(true);
                break;
            }
            else Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].set_check_flag(true);
        }
    }
}

Bishop::~Bishop()
{

}
