#include "widget.h"
#include "ui_widget.h"
#include "game.h"

#include <QFont>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::setup_dead_pieces()
{
    QSize dead_tile(12, 12);
    for (int index = 0; index < 16; index++)
    {
        Game::get_game_instance()->white_dead_pieces[index].setIconSize(dead_tile);
        ui->white_dead_pieces->addWidget(&Game::get_game_instance()->white_dead_pieces[index], index, 0);

        Game::get_game_instance()->black_dead_pieces[index].setIconSize(dead_tile);
        ui->black_dead_pieces->addWidget(&Game::get_game_instance()->black_dead_pieces[index], index, 0);

        QPixmap default_dead_tile("default_dead_tile.png");
        QIcon DeadTile(default_dead_tile);
        Game::get_game_instance()->white_dead_pieces[index].setIcon(DeadTile);
        Game::get_game_instance()->black_dead_pieces[index].setIcon(DeadTile);
    }
}

void Widget::setup_buttons()
{
    QSize rect_tile(55, 55);
    for (int row = 0; row < BORDER; row++)
    {
        for (int column = 0; column < BORDER; column++)
        {
            Game::get_game_instance()->tile[row][column].setIconSize(rect_tile);
            ui->board_layout->addWidget(&Game::get_game_instance()->tile[row][column], row, column, 0);
        }
    }
}

void Widget::set_player_names()
{
    ui->player1->setText(Game::get_game_instance()->player1_name);
    ui->player2->setText(Game::get_game_instance()->player2_name);
}

void Widget::debug(int k, int q)
{
    QString asghar = QString::number(k);
    QString akbar = QString::number(q);
    this->ui->blahblah->setText(asghar);
    this->ui->blahblah2->setText(akbar);
}

void Widget::set_bold_player1(bool is_true)
{
    QFont is_not_turn = QFont("Garamond");
    is_not_turn.setBold(false);
    is_not_turn.setPixelSize(14);
    QFont is_turn = QFont("Garamond");
    is_turn.setBold(true);
    is_turn.setPixelSize(14);
    if (is_true)
    {
        ui->player1->setFont(is_turn);
        ui->player2->setFont(is_not_turn);
    }
    else
    {
        ui->player1->setFont(is_not_turn);
        ui->player2->setFont(is_turn);
    }
}
