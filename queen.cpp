#include "queen.h"
#include "game.h"

Queen::Queen(int row, int column, bool is_white, bool is_at_initial_position)
{
    if (is_white)
    {
        QPixmap white_queen("white_queen.png");
        QIcon WhiteQueen(white_queen);
        Game::get_game_instance()->set_icon(WhiteQueen, row, column);
        this->is_white = true;
    }

    if (!is_white)
    {
        QPixmap black_queen("black_queen.png");
        QIcon BlackQueen(black_queen);
        Game::get_game_instance()->set_icon(BlackQueen, row, column);
        this->is_white = false;
    }
    this->position[0] = row;
    this->position[1] = column;
    this->is_alive = true;
    this->is_at_initial_position = is_at_initial_position;
    this->piece_type = "Queen";
}

QIcon Queen::white_icon()
{
    QPixmap white_queen("white_queen.png");
    QIcon WhiteQueen(white_queen);
    return WhiteQueen;
}

QIcon Queen::white_selected_icon()
{
    QPixmap white_selected_queen("selected_white_queen.png");
    QIcon WhiteSelectedQueen(white_selected_queen);
    return WhiteSelectedQueen;
}

QIcon Queen::black_icon()
{
    QPixmap black_queen("black_queen.png");
    QIcon BlackQueen(black_queen);
    return BlackQueen;
}

QIcon Queen::black_selected_icon()
{
    QPixmap black_selected_queen("selected_black_queen.png");
    QIcon BlackSelectedQueen(black_selected_queen);
    return BlackSelectedQueen;
}

QIcon Queen::white_kill_icon()
{
    QPixmap white_kill_queen("kill_white_queen.png");
    QIcon WhiteKillQueen(white_kill_queen);
    return WhiteKillQueen;
}

QIcon Queen::black_kill_icon()
{
    QPixmap black_kill_queen("kill_black_queen.png");
    QIcon BlackKillQueen(black_kill_queen);
    return BlackKillQueen;
}

void Queen::set_position(int row, int column)
{
    position[0] = row;
    position[1] = column;
}

void Queen::highlight_possible_moves()
{
    int row = this->position[0];
    int column = this->position[1];

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row + diagonal_length) >= BORDER || (row + diagonal_length) < 0  ||
                (column + diagonal_length) >= BORDER || (column + diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].make_possible_kill("0");
                    }
                }
                break;
            }
            else
            {
                if (!Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].make_possible_move();
                }
            }
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row - diagonal_length) >= BORDER || (row - diagonal_length) < 0  ||
                (column + diagonal_length) >= BORDER || (column + diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].make_possible_kill("0");
                    }
                }
                break;
            }
            else
            {
                if (!Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].make_possible_move();
                }
            }
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row + diagonal_length) >= BORDER || (row + diagonal_length) < 0  ||
                (column - diagonal_length) >= BORDER || (column - diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].make_possible_kill("0");
                    }
                }
                break;
            }
            else
            {
                if (!Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].make_possible_move();
                }
            }
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row - diagonal_length) >= BORDER || (row - diagonal_length) < 0  ||
                (column - diagonal_length) >= BORDER || (column - diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].make_possible_kill("0");
                    }
                }
                break;
            }
            else
            {
                if (!Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].make_possible_move();
                }
            }
        }
    }


    for (int side_length = 1; ; side_length++)
    {
        if (row + side_length >= BORDER || row + side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row + side_length][column].tile_address())
            {
                if (!Game::get_game_instance()->tile[row + side_length][column].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row + side_length][column].make_possible_move();
                }
            }
            else
            {
                if (Game::get_game_instance()->tile[row + side_length][column].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row + side_length][column].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row + side_length][column].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row + side_length][column].make_possible_kill("0");
                    }
                }
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (row - side_length >= BORDER || row - side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row - side_length][column].tile_address())
            {
                if (!Game::get_game_instance()->tile[row - side_length][column].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row - side_length][column].make_possible_move();
                }
            }
            else
            {
                if (Game::get_game_instance()->tile[row - side_length][column].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row - side_length][column].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row - side_length][column].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row - side_length][column].make_possible_kill("0");
                    }
                }
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (column + side_length >= BORDER || column + side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row][column + side_length].tile_address())
            {
                if (!Game::get_game_instance()->tile[row][column + side_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row][column + side_length].make_possible_move();
                }
            }
            else
            {
                if (Game::get_game_instance()->tile[row][column + side_length].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row][column + side_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row][column + side_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row][column + side_length].make_possible_kill("0");
                    }
                }
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (column - side_length >= BORDER || column - side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row][column - side_length].tile_address())
            {
                if (!Game::get_game_instance()->tile[row][column - side_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row][column - side_length].make_possible_move();
                }
            }
            else
            {
                if (Game::get_game_instance()->tile[row][column - side_length].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row][column - side_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row][column - side_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row][column - side_length].make_possible_kill("0");
                    }
                }
                break;
            }
        }
    }
}

void Queen::flag_checked_tiles()
{
    int row = this->position[0];
    int column = this->position[1];

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row + diagonal_length) >= BORDER || (row + diagonal_length) < 0  ||
                (column + diagonal_length) >= BORDER || (column + diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].set_check_flag(true);
                break;
            }
            else Game::get_game_instance()->tile[row + diagonal_length][column + diagonal_length].set_check_flag(true);
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row - diagonal_length) >= BORDER || (row - diagonal_length) < 0  ||
                (column + diagonal_length) >= BORDER || (column + diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].set_check_flag(true);
                break;
            }
            else Game::get_game_instance()->tile[row - diagonal_length][column + diagonal_length].set_check_flag(true);
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row + diagonal_length) >= BORDER || (row + diagonal_length) < 0  ||
                (column - diagonal_length) >= BORDER || (column - diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].set_check_flag(true);
                break;
            }
            else Game::get_game_instance()->tile[row + diagonal_length][column - diagonal_length].set_check_flag(true);
        }
    }

    for (int diagonal_length = 1; ; diagonal_length++)
    {
        if ((row - diagonal_length) >= BORDER || (row - diagonal_length) < 0  ||
                (column - diagonal_length) >= BORDER || (column - diagonal_length) < 0)
            break;
        else
        {
            if (Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].tile_address())
            {
                if (Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_white() && !this->is_white
                        ||! Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].set_check_flag(true);
                break;
            }
            else Game::get_game_instance()->tile[row - diagonal_length][column - diagonal_length].set_check_flag(true);
        }
    }

    for (int side_length = 1; ; side_length++)
    {
        if (row + side_length >= BORDER || row + side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row + side_length][column].tile_address())
                Game::get_game_instance()->tile[row + side_length][column].set_check_flag(true);
            else
            {
                if (Game::get_game_instance()->tile[row + side_length][column].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row + side_length][column].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row + side_length][column].set_check_flag(true);
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (row - side_length >= BORDER || row - side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row - side_length][column].tile_address())
                Game::get_game_instance()->tile[row - side_length][column].set_check_flag(true);
            else
            {
                if (Game::get_game_instance()->tile[row - side_length][column].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row - side_length][column].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row - side_length][column].set_check_flag(true);
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (column + side_length >= BORDER || column + side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row][column + side_length].tile_address())
                Game::get_game_instance()->tile[row][column + side_length].set_check_flag(true);
            else
            {
                if (Game::get_game_instance()->tile[row][column + side_length].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row][column + side_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row][column + side_length].set_check_flag(true);
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (column - side_length >= BORDER || column - side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row][column - side_length].tile_address())
                Game::get_game_instance()->tile[row][column - side_length].set_check_flag(true);
            else
            {
                if (Game::get_game_instance()->tile[row][column - side_length].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row][column - side_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row][column - side_length].set_check_flag(true);
                break;
            }
        }
    }
}

Queen::~Queen()
{

}
