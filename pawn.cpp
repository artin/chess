#include "pawn.h"
#include "game.h"

Pawn::Pawn(int row, int column, bool is_white, bool is_at_initial_position)
{
    if (is_white)
    {
        Game::get_game_instance()->set_icon(white_icon(), row, column);
    }

    if (!is_white)
    {
        Game::get_game_instance()->set_icon(black_icon(), row, column);
    }

    this->is_white = is_white;
    this->position[0] = row;
    this->position[1] = column;
    this->is_alive = true;
    this->is_at_initial_position = is_at_initial_position;
    this->piece_type = "Pawn";
}

QIcon Pawn::white_icon()
{
    QPixmap white_pawn("white_pawn.png");
    QIcon WhitePawn(white_pawn);
    return WhitePawn;
}

QIcon Pawn::white_selected_icon()
{
    QPixmap white_selected_pawn("selected_white_pawn.png");
    QIcon WhiteSelectedPawn(white_selected_pawn);
    return WhiteSelectedPawn;
}

QIcon Pawn::black_icon()
{
    QPixmap black_pawn("black_pawn.png");
    QIcon BlackPawn(black_pawn);
    return BlackPawn;
}

QIcon Pawn::black_selected_icon()
{
    QPixmap black_selected_pawn("selected_black_pawn.png");
    QIcon BlackSelectedPawn(black_selected_pawn);
    return BlackSelectedPawn;
}

QIcon Pawn::white_kill_icon()
{
    QPixmap white_kill_pawn("kill_white_pawn.png");
    QIcon WhiteKillPawn(white_kill_pawn);
    return WhiteKillPawn;
}

QIcon Pawn::black_kill_icon()
{
    QPixmap black_kill_pawn("kill_black_pawn.png");
    QIcon BlackKillPawn(black_kill_pawn);
    return BlackKillPawn;
}

void Pawn::set_position(int row, int column)
{
    this->position[0] = row;
    this->position[1] = column;
}

void Pawn::highlight_possible_moves()
{
    int possible_position[2];
    if (this->is_white)
    {
        if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].tile_address() &&
                this->position[0]-1 >= 0 && this->position[0]-1 < BORDER)
        {

            possible_position[0] = this->position[0] - 1;
            possible_position[1] = this->position[1];

            if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].make_possible_move();
                if ((this->is_white && this->position[0] == 6)|| (!this->is_white && this->position[0] == 1))
                    this->is_at_initial_position = true;
                else this->is_at_initial_position = false;
            }
        }

        if (this->is_at_initial_position && Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].is_a_possible_move)
        {
            if (!Game::get_game_instance()->tile[this->position[0]-2][this->position[1]].tile_address())
            {
                if (!Game::get_game_instance()->tile[this->position[0]-2][this->position[1]].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[this->position[0]-2][this->position[1]].make_possible_move();
                    if ((this->is_white && this->position[0] == 6)|| (!this->is_white && this->position[0] == 1))
                        this->is_at_initial_position = true;
                    else this->is_at_initial_position = false;
                }
            }
        }
//        if (this->position[0]-1 >= 0 && this->position[0]-1 < BORDER && this->position[1]-1 >= 0 && this->position[1]-1 < BORDER)
//        {
//            if (Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].has_a_piece())
//            {
//                if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_white())
//                {
////                    if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_a_bad_move(this->position))
//                    {
//                        Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].make_possible_kill("0");
//                    }
//                }
//            }
//        }
        if (this->position[0]-1 >= 0 && this->position[0]-1 < BORDER && this->position[1]-1 >= 0 && this->position[1]-1 < BORDER)
        {
            if (Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].has_a_piece())
            {
                if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_white())
                {
                    if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].make_possible_kill("0");
                    }
                }
            }
        }
        if (this->position[0]-1 >= 0 && this->position[0]-1 < BORDER && this->position[1]+1 >= 0 && this->position[1]+1 < BORDER)
        {
            if (Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].has_a_piece())
            {
                if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].is_white())
                {
                    if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].make_possible_kill("0");
                    }
                }
            }
        }
    }
    if (!this->is_white)
    {
        if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].tile_address() &&
                this->position[0] - 1 >= 0 && this->position[0] - 1 < BORDER)
        {
            if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].make_possible_move();
                if ((this->is_white && this->position[0] == 6)|| (!this->is_white && this->position[0] == 1))
                    this->is_at_initial_position = true;
                else this->is_at_initial_position = false;
            }
        }
        if (this->is_at_initial_position && Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].is_a_possible_move)
        {
            if (!Game::get_game_instance()->tile[this->position[0]+2][this->position[1]].tile_address())
            {
                if (!Game::get_game_instance()->tile[this->position[0]+2][this->position[1]].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[this->position[0]+2][this->position[1]].make_possible_move();
                    if ((this->is_white && this->position[0] == 6)|| (!this->is_white && this->position[0] == 1))
                        this->is_at_initial_position = true;
                    else this->is_at_initial_position = false;
                }
            }
        }
        if (this->position[0]+1 >= 0 && this->position[0]+1 < BORDER && this->position[1]-1 >= 0 && this->position[1]-1 < BORDER)
        {
            if (Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].tile_address())
            {
                if (Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].is_white())
                {
                    if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].make_possible_kill("0");
                    }
                }
            }
        }
        if (this->position[0]+1 >= 0 && this->position[0]+1 < BORDER && this->position[1]+1 >= 0 && this->position[1]+1 < BORDER)
        {
            if (Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].tile_address())
            {
                if (Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].is_white())
                {
                    if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].make_possible_kill("0");
                    }
                }
            }
        }
    }
//    if ((this->is_white && this->position[0] == 6) || (!(this->is_white) && this->position[0] == 1))
//        this->is_at_initial_position = true;
//    else this->is_at_initial_position = false;
}

void Pawn::flag_checked_tiles()
{
    if (this->is_white)
    {
        if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].tile_address() &&
                this->position[0]-1 >= 0 && this->position[0]-1 < BORDER)
            Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].set_check_flag(true);
        if (this->is_at_initial_position && Game::get_game_instance()->tile[this->position[0]-1][this->position[1]].is_a_possible_move)
        {
            if (!Game::get_game_instance()->tile[this->position[0]-2][this->position[1]].tile_address())
                Game::get_game_instance()->tile[this->position[0]-2][this->position[1]].set_check_flag(true);
        }
        if (this->position[0]-1 >= 0 && this->position[0]-1 < BORDER && this->position[1]-1 >= 0 && this->position[1]-1 < BORDER)
        {
            if (Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].has_a_piece())
            {
                if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].is_white())
                    Game::get_game_instance()->tile[this->position[0]-1][this->position[1]-1].set_check_flag(true);
            }
        }
        if (this->position[0]-1 >= 0 && this->position[0]-1 < BORDER && this->position[1]+1 >= 0 && this->position[1]+1 < BORDER)
        {
            if (Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].has_a_piece())
            {
                if (!Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].is_white())
                    Game::get_game_instance()->tile[this->position[0]-1][this->position[1]+1].set_check_flag(true);
            }
        }
    }
    if (!this->is_white)
    {
        if (!Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].tile_address() &&
                this->position[0] - 1 >= 0 && this->position[0] - 1 < BORDER)
            Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].set_check_flag(true);
        if (this->is_at_initial_position && Game::get_game_instance()->tile[this->position[0]+1][this->position[1]].is_a_possible_move)
        {
            if (!Game::get_game_instance()->tile[this->position[0]+2][this->position[1]].tile_address())
                Game::get_game_instance()->tile[this->position[0]+2][this->position[1]].set_check_flag(true);
        }
        if (this->position[0]+1 >= 0 && this->position[0]+1 < BORDER && this->position[1]-1 >= 0 && this->position[1]-1 < BORDER)
        {
            if (Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].tile_address())
            {
                if (Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].is_white())
                    Game::get_game_instance()->tile[this->position[0]+1][this->position[1]-1].set_check_flag(true);
            }
        }
        if (this->position[0]+1 >= 0 && this->position[0]+1 < BORDER && this->position[1]+1 >= 0 && this->position[1]+1 < BORDER)
        {
            if (Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].tile_address())
            {
                if (Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].is_white())
                    Game::get_game_instance()->tile[this->position[0]+1][this->position[1]+1].set_check_flag(true);
            }
        }
    }
}

Pawn::~Pawn()
{

}
