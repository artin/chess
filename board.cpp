#include "tile.h"

Tile::Tile(QPushButton *parent) :
    QPushButton(parent)
{
    //is_selected = false;
}

void Tile::set_position(int input_row, int input_column)
{
    position[0] = input_column;
    position[1] = input_row;
}

void Tile::select()
{
    is_selected = true;
}

void Tile::deselect()
{
    is_selected = false;
}
