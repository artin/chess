#include "knight.h"
#include "game.h"

Knight::Knight(int row, int column, bool is_white, bool is_at_initial_position)
{
    if (is_white)
    {
        Game::get_game_instance()->set_icon(white_icon(), row, column);
        this->is_white = true;
    }

    if (!is_white)
    {
        Game::get_game_instance()->set_icon(black_icon(), row, column);
        this->is_white = false;
    }
    this->position[0] = row;
    this->position[1] = column;
    this->is_alive = true;
    this->is_at_initial_position = is_at_initial_position;
    this->piece_type = "Knight";
}

QIcon Knight::white_icon()
{
    QPixmap white_knight("white_knight.png");
    QIcon WhiteKnight(white_knight);
    return WhiteKnight;
}

QIcon Knight::white_selected_icon()
{
    QPixmap white_selected_knight("selected_white_knight.png");
    QIcon WhiteSelectedKnight(white_selected_knight);
    return WhiteSelectedKnight;
}

QIcon Knight::black_icon()
{
    QPixmap black_knight("black_knight.png");
    QIcon BlackKnight(black_knight);
    return BlackKnight;
}

QIcon Knight::black_selected_icon()
{
    QPixmap black_selected_knight("selected_black_knight.png");
    QIcon BlackSelectedKnight(black_selected_knight);
    return BlackSelectedKnight;
}

QIcon Knight::white_kill_icon()
{
    QPixmap white_kill_knight("kill_white_knight.png");
    QIcon WhiteKillKnight(white_kill_knight);
    return WhiteKillKnight;
}

QIcon Knight::black_kill_icon()
{
    QPixmap black_kill_knight("kill_black_knight.png");
    QIcon BlackKillKnight(black_kill_knight);
    return BlackKillKnight;
}

void Knight::set_position(int row, int column)
{
    position[0] = row;
    position[1] = column;
}

void Knight::highlight_possible_moves()
{
    if (this->position[0]-2 < BORDER && this->position[0]-2 >= 0 && this->position[1]+1 < BORDER && this->position[1]+1 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]-2][position[1]+1].tile_address())
        {
            if (!Game::get_game_instance()->tile[position[0]-2][position[1]+1].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[position[0]-2][position[1]+1].make_possible_move();
            }
        }
        else
        {
            if (Game::get_game_instance()->tile[position[0]-2][position[1]+1].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]-2][position[1]+1].is_white() && this->is_white)
            {
                if (!Game::get_game_instance()->tile[position[0]-2][position[1]+1].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[position[0]-2][position[1]+1].make_possible_kill("0");
                }
            }
        }
    }

    if (this->position[0]-1 < BORDER && this->position[0]-1 >= 0 && this->position[1]+2 < BORDER && this->position[1]+2 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]-1][position[1]+2].tile_address())
        {
            if (!Game::get_game_instance()->tile[position[0]-1][position[1]+2].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[position[0]-1][position[1]+2].make_possible_move();
            }
        }
        else
        {
            if (Game::get_game_instance()->tile[position[0]-1][position[1]+2].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]-1][position[1]+2].is_white() && this->is_white)
            {
                if (!Game::get_game_instance()->tile[position[0]-1][position[1]+2].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[position[0]-1][position[1]+2].make_possible_kill("0");
                }
            }
        }
    }

    if (this->position[0]+1 < BORDER && this->position[0]+1 >= 0 && this->position[1]+2 < BORDER && this->position[1]+2 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]+1][position[1]+2].tile_address())
        {
            if (!Game::get_game_instance()->tile[position[0]+1][position[1]+2].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[position[0]+1][position[1]+2].make_possible_move();
            }
        }
        else
        {
            if (Game::get_game_instance()->tile[position[0]+1][position[1]+2].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]+1][position[1]+2].is_white() && this->is_white)
            {
                if (!Game::get_game_instance()->tile[position[0]+1][position[1]+2].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[position[0]+1][position[1]+2].make_possible_kill("0");
                }
            }
        }
    }

    if (this->position[0]+2 < BORDER && this->position[0]+2 >= 0 && this->position[1]+1 < BORDER && this->position[1]+1 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]+2][position[1]+1].tile_address())
        {
            if (!Game::get_game_instance()->tile[position[0]+2][position[1]+1].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[position[0]+2][position[1]+1].make_possible_move();
            }
        }
        else
        {
            if (Game::get_game_instance()->tile[position[0]+2][position[1]+1].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]+2][position[1]+1].is_white() && this->is_white)
            {
                if (!Game::get_game_instance()->tile[position[0]+2][position[1]+1].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[position[0]+2][position[1]+1].make_possible_kill("0");
                }
            }
        }
    }

    if (this->position[0]+2 < BORDER && this->position[0]+2 >= 0 && this->position[1]-1 < BORDER && this->position[1]-1 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]+2][position[1]-1].tile_address())
        {
            if (!Game::get_game_instance()->tile[position[0]+2][position[1]-1].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[position[0]+2][position[1]-1].make_possible_move();
            }
        }
        else
        {
            if (Game::get_game_instance()->tile[position[0]+2][position[1]-1].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]+2][position[1]-1].is_white() && this->is_white)
            {
                if (!Game::get_game_instance()->tile[position[0]+2][position[1]-1].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[position[0]+2][position[1]-1].make_possible_kill("0");
                }
            }
        }
    }

    if (this->position[0]-1 < BORDER && this->position[0]-1 >= 0 && this->position[1]-2 < BORDER && this->position[1]-2 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]-1][position[1]-2].tile_address())
        {
            if (!Game::get_game_instance()->tile[position[0]-1][position[1]-2].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[position[0]-1][position[1]-2].make_possible_move();
            }
        }
        else
        {
            if (Game::get_game_instance()->tile[position[0]-1][position[1]-2].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]-1][position[1]-2].is_white() && this->is_white)
            {
                if (!Game::get_game_instance()->tile[position[0]-1][position[1]-2].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[position[0]-1][position[1]-2].make_possible_kill("0");
                }
            }
        }
    }

    if (this->position[0]+1 < BORDER && this->position[0]+1 >= 0 && this->position[1]-2 < BORDER && this->position[1]-2 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]+1][position[1]-2].tile_address())
        {
            if (!Game::get_game_instance()->tile[position[0]+1][position[1]-2].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[position[0]+1][position[1]-2].make_possible_move();
            }
        }
        else
        {
            if (Game::get_game_instance()->tile[position[0]+1][position[1]-2].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]+1][position[1]-2].is_white() && this->is_white)
            {
                if (!Game::get_game_instance()->tile[position[0]+1][position[1]-2].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[position[0]+1][position[1]-2].make_possible_kill("0");
                }
            }
        }
    }

    if (this->position[0]-2 < BORDER && this->position[0]-2 >= 0 && this->position[1]-1 < BORDER && this->position[1]-1 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]-2][position[1]-1].tile_address())
        {
            if (!Game::get_game_instance()->tile[position[0]-2][position[1]-1].is_a_bad_move(this->position))
            {
                Game::get_game_instance()->tile[position[0]-2][position[1]-1].make_possible_move();
            }
        }
        else
        {
            if (Game::get_game_instance()->tile[position[0]-2][position[1]-1].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]-2][position[1]-1].is_white() && this->is_white)
            {
                if (!Game::get_game_instance()->tile[position[0]-2][position[1]-1].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[position[0]-2][position[1]-1].make_possible_kill("0");
                }
            }
        }
    }
}

void Knight::flag_checked_tiles()
{
    if (this->position[0]-2 < BORDER && this->position[0]-2 >= 0 && this->position[1]+1 < BORDER && this->position[1]+1 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]-2][position[1]+1].tile_address())
            Game::get_game_instance()->tile[position[0]-2][position[1]+1].set_check_flag(true);
        else
        {
            if (Game::get_game_instance()->tile[position[0]-2][position[1]+1].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]-2][position[1]+1].is_white() && this->is_white)
                Game::get_game_instance()->tile[position[0]-2][position[1]+1].set_check_flag(true);
        }
    }

    if (this->position[0]-1 < BORDER && this->position[0]-1 >= 0 && this->position[1]+2 < BORDER && this->position[1]+2 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]-1][position[1]+2].tile_address())
            Game::get_game_instance()->tile[position[0]-1][position[1]+2].set_check_flag(true);
        else
        {
            if (Game::get_game_instance()->tile[position[0]-1][position[1]+2].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]-1][position[1]+2].is_white() && this->is_white)
                Game::get_game_instance()->tile[position[0]-1][position[1]+2].set_check_flag(true);
        }
    }

    if (this->position[0]+1 < BORDER && this->position[0]+1 >= 0 && this->position[1]+2 < BORDER && this->position[1]+2 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]+1][position[1]+2].tile_address())
            Game::get_game_instance()->tile[position[0]+1][position[1]+2].set_check_flag(true);
        else
        {
            if (Game::get_game_instance()->tile[position[0]+1][position[1]+2].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]+1][position[1]+2].is_white() && this->is_white)
                Game::get_game_instance()->tile[position[0]+1][position[1]+2].set_check_flag(true);
        }
    }

    if (this->position[0]+2 < BORDER && this->position[0]+2 >= 0 && this->position[1]+1 < BORDER && this->position[1]+1 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]+2][position[1]+1].tile_address())
            Game::get_game_instance()->tile[position[0]+2][position[1]+1].set_check_flag(true);
        else
        {
            if (Game::get_game_instance()->tile[position[0]+2][position[1]+1].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]+2][position[1]+1].is_white() && this->is_white)
                Game::get_game_instance()->tile[position[0]+2][position[1]+1].set_check_flag(true);
        }
    }

    if (this->position[0]+2 < BORDER && this->position[0]+2 >= 0 && this->position[1]-1 < BORDER && this->position[1]-1 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]+2][position[1]-1].tile_address())
            Game::get_game_instance()->tile[position[0]+2][position[1]-1].set_check_flag(true);
        else
        {
            if (Game::get_game_instance()->tile[position[0]+2][position[1]-1].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]+2][position[1]-1].is_white() && this->is_white)
                Game::get_game_instance()->tile[position[0]+2][position[1]-1].set_check_flag(true);
        }
    }

    if (this->position[0]-1 < BORDER && this->position[0]-1 >= 0 && this->position[1]-2 < BORDER && this->position[1]-2 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]-1][position[1]-2].tile_address())
            Game::get_game_instance()->tile[position[0]-1][position[1]-2].set_check_flag(true);
        else
        {
            if (Game::get_game_instance()->tile[position[0]-1][position[1]-2].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]-1][position[1]-2].is_white() && this->is_white)
                Game::get_game_instance()->tile[position[0]-1][position[1]-2].set_check_flag(true);
        }
    }

    if (this->position[0]+1 < BORDER && this->position[0]+1 >= 0 && this->position[1]-2 < BORDER && this->position[1]-2 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]+1][position[1]-2].tile_address())
            Game::get_game_instance()->tile[position[0]+1][position[1]-2].set_check_flag(true);
        else
        {
            if (Game::get_game_instance()->tile[position[0]+1][position[1]-2].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]+1][position[1]-2].is_white() && this->is_white)
                Game::get_game_instance()->tile[position[0]+1][position[1]-2].set_check_flag(true);
        }
    }

    if (this->position[0]-2 < BORDER && this->position[0]-2 >= 0 && this->position[1]-1 < BORDER && this->position[1]-1 >= 0)
    {
        if (!Game::get_game_instance()->tile[position[0]-2][position[1]-1].tile_address())
            Game::get_game_instance()->tile[position[0]-2][position[1]-1].set_check_flag(true);
        else
        {
            if (Game::get_game_instance()->tile[position[0]-2][position[1]-1].is_white() && !this->is_white ||
                    !Game::get_game_instance()->tile[position[0]-2][position[1]-1].is_white() && this->is_white)
                Game::get_game_instance()->tile[position[0]-2][position[1]-1].set_check_flag(true);
        }
    }
}

Knight::~Knight()
{

}
