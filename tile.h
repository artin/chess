#ifndef TILE_H
#define TILE_H

#include <QPushButton>
#include "piece.h"

class Tile : public QPushButton
{
    friend class Game;
    friend class Pawn;
    friend class Rook;
    friend class Knight;
    friend class Bishop;
    friend class Queen;
    friend class King;
    Q_OBJECT
public:
    explicit Tile(QPushButton *parent = 0);
    void set_position(int, int);
    void select(bool);
    void set_icon(QIcon, int, int);
    void mousePressEvent(QMouseEvent *);
    void enable_tiles(bool);
    void make_possible_move();
    void unmake_possible_move();
    void make_possible_kill(string);
    void set_default_tile();
    void swap(int *, bool, string);
    void swap_kill(int *, bool, string);
    void set_possible_move(bool);
    void set_possible_kill(bool);
    void set_initial_position();
    bool is_white();
    bool has_a_piece();
    void set_check_flag(bool);
    bool is_a_bad_move(int *);
    Piece *tile_address();

protected:
    //Tile* return_address();

signals:

public slots:

private:
    int position[2];
    bool is_selected;
    bool is_a_possible_move;
    bool is_a_possible_kill;
    Piece *piece;
    bool is_selectable;
    bool check_flag;
};

#endif // TILE_H
