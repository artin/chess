#include "rook.h"
#include "game.h"

Rook::Rook(int row, int column, bool is_white, bool is_at_initial_position)
{
    if (is_white)
    {
        QPixmap white_rook("white_rook.png");
        QIcon WhiteRook(white_rook);
        Game::get_game_instance()->set_icon(WhiteRook, row, column);
        this->is_white = true;
    }

    if (!is_white)
    {
        QPixmap black_rook("black_rook.png");
        QIcon BlackRook(black_rook);
        Game::get_game_instance()->set_icon(BlackRook, row, column);
        this->is_white = false;
    }
    this->position[0] = row;
    this->position[1] = column;
    this->is_alive = true;
    this->is_at_initial_position = is_at_initial_position;
    this->piece_type = "Rook";
}

QIcon Rook::white_icon()
{
    QPixmap white_rook("white_rook.png");
    QIcon WhiteRook(white_rook);
    return WhiteRook;
}

QIcon Rook::white_selected_icon()
{
    QPixmap white_selected_rook("selected_white_rook.png");
    QIcon WhiteSelectedRook(white_selected_rook);
    return WhiteSelectedRook;
}

QIcon Rook::black_icon()
{
    QPixmap black_rook("black_rook.png");
    QIcon BlackRook(black_rook);
    return BlackRook;
}

QIcon Rook::black_selected_icon()
{
    QPixmap black_selected_rook("selected_black_rook.png");
    QIcon BlackSelectedRook(black_selected_rook);
    return BlackSelectedRook;
}

QIcon Rook::white_kill_icon()
{
    QPixmap white_kill_rook("kill_white_rook.png");
    QIcon WhiteKillRook(white_kill_rook);
    return WhiteKillRook;
}

QIcon Rook::black_kill_icon()
{
    QPixmap black_kill_rook("kill_black_rook.png");
    QIcon BlackKillRook(black_kill_rook);
    return BlackKillRook;
}

void Rook::set_position(int row, int column)
{
    position[0] = row;
    position[1] = column;
}

void Rook::highlight_possible_moves()
{
    int row = this->position[0];
    int column = this->position[1];
    for (int side_length = 1; ; side_length++)
    {
        if (row + side_length >= BORDER || row + side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row + side_length][column].tile_address())
            {
                if (!Game::get_game_instance()->tile[row + side_length][column].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row + side_length][column].make_possible_move();
                }
            }
            else
            {
                if (Game::get_game_instance()->tile[row + side_length][column].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row + side_length][column].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row + side_length][column].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row + side_length][column].make_possible_kill("0");
                    }
                }
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (row - side_length >= BORDER || row - side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row - side_length][column].tile_address())
            {
                if (!Game::get_game_instance()->tile[row - side_length][column].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row - side_length][column].make_possible_move();
                }
            }
            else
            {
                if (Game::get_game_instance()->tile[row - side_length][column].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row - side_length][column].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row - side_length][column].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row - side_length][column].make_possible_kill("0");
                    }
                }
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (column + side_length >= BORDER || column + side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row][column + side_length].tile_address())
            {
                if (!Game::get_game_instance()->tile[row][column + side_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row][column + side_length].make_possible_move();
                }
            }
            else
            {
                if (Game::get_game_instance()->tile[row][column + side_length].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row][column + side_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row][column + side_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row][column + side_length].make_possible_kill("0");
                    }
                }
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (column - side_length >= BORDER || column - side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row][column - side_length].tile_address())
            {
                if (!Game::get_game_instance()->tile[row][column - side_length].is_a_bad_move(this->position))
                {
                    Game::get_game_instance()->tile[row][column - side_length].make_possible_move();
                }
            }
            else
            {
                if (Game::get_game_instance()->tile[row][column - side_length].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row][column - side_length].is_white() && this->is_white)
                {
                    if (!Game::get_game_instance()->tile[row][column - side_length].is_a_bad_move(this->position))
                    {
                        Game::get_game_instance()->tile[row][column - side_length].make_possible_kill("0");
                    }
                }
                break;
            }
        }
    }
}

void Rook::flag_checked_tiles()
{
    int row = this->position[0];
    int column = this->position[1];
    for (int side_length = 1; ; side_length++)
    {
        if (row + side_length >= BORDER || row + side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row + side_length][column].tile_address())
                Game::get_game_instance()->tile[row + side_length][column].set_check_flag(true);
            else
            {
                if (Game::get_game_instance()->tile[row + side_length][column].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row + side_length][column].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row + side_length][column].set_check_flag(true);
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (row - side_length >= BORDER || row - side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row - side_length][column].tile_address())
                Game::get_game_instance()->tile[row - side_length][column].set_check_flag(true);
            else
            {
                if (Game::get_game_instance()->tile[row - side_length][column].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row - side_length][column].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row - side_length][column].set_check_flag(true);
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (column + side_length >= BORDER || column + side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row][column + side_length].tile_address())
                Game::get_game_instance()->tile[row][column + side_length].set_check_flag(true);
            else
            {
                if (Game::get_game_instance()->tile[row][column + side_length].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row][column + side_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row][column + side_length].set_check_flag(true);
                break;
            }
        }
    }
    for (int side_length = 1; ; side_length++)
    {
        if (column - side_length >= BORDER || column - side_length < 0)
            break;
        else
        {
            if (!Game::get_game_instance()->tile[row][column - side_length].tile_address())
                Game::get_game_instance()->tile[row][column - side_length].set_check_flag(true);
            else
            {
                if (Game::get_game_instance()->tile[row][column - side_length].is_white() && !this->is_white ||
                        !Game::get_game_instance()->tile[row][column - side_length].is_white() && this->is_white)
                    Game::get_game_instance()->tile[row][column - side_length].set_check_flag(true);
                break;
            }
        }
    }
}

Rook::~Rook()
{

}
